const path = require('path');
const fs = require('fs');
const { setup, styles, javascript, browsersync, images, monitor } = require('argonauts');
const { parallel, series } = require('gulp');
const config = require('./config.js');
const seriesTasks = [];
const parallelTasks = [];

setup(config);

if (config.url !== null) {
    seriesTasks.push(browsersync);
}

if (config.css !== null) {
    parallelTasks.push(styles);
}

if (config.js !== null) {
    parallelTasks.push(javascript);
}

if (config.img !== null) {
    parallelTasks.push(images);
}

if (parallelTasks.length > 0) {
    seriesTasks.push(parallel(...parallelTasks));
}

if (config.url !== null) {
    seriesTasks.push(monitor);
}

exports.default = series(...seriesTasks);
